use repo_link;
use std::env;
use std::process;

use repo_link::Config;

fn main() {
    let args: Vec<String> = env::args().collect();

    let config = Config::new(&args).unwrap_or_else(|err| {
        eprintln!("{}", err);
        process::exit(1)
    });

    let result = repo_link::run(config).unwrap_or_else(|err| {
        eprintln!("{}", err);
        process::exit(1)
    });

    println!("{}", result);
}
